﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AliveByContact : MonoBehaviour {
	public GameObject explosion;
	public int scoreValue;
	private GameController gameController;

	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindGameObjectWithTag ("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent <GameController>();
		}
		if (gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Boundary" || other.tag == "Friend")
		{
			return;
		}

		if (explosion != null)
		{
			Instantiate (explosion, transform.position, transform.rotation);

		}
		if (other.tag == "Bolt") 
		{
			gameController.AddScore (scoreValue);
			Destroy (other.gameObject);
		}
		if (other.tag == "Player") 
		{
			gameController.AddScore (scoreValue);
		}
		
		Destroy (gameObject);
	}
}